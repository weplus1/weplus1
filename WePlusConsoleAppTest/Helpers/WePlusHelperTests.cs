﻿using Microsoft.Extensions.Configuration;
using WePlusConsoleApp.Helpers;

namespace WePlusConsoleAppTest.Helpers
{
    [TestClass]
    public class WePlusHelperTests
    {
        WePlusHelper? wePlusHelper;

        [TestInitialize]
        public void Initialize()
        {
            var config = new ConfigurationBuilder()
               .AddJsonFile("appsettings.test.json")
                .Build();
            wePlusHelper = new WePlusHelper(config);
        }

        [TestMethod]
        public async Task Configuration_GetCities_Test()
        {
            var cities = wePlusHelper?.GetCities();

            Assert.IsNotNull(cities,
                "City list cannot be empty");

            var cityList = new List<string>();

            await foreach (var city in cities)
            {
                cityList.Add(city);
            }

            if (cities != null)
            {
                Assert.IsTrue(string.Join(',', cityList) == "Singapore,Perth,Auckland",
                    $"Expected value 'Singapore,Perth,Auckland', Actual value '{string.Join(',', cityList)}'");
            }
        }

        [TestMethod]
        public void Configuration_Interval_Test()
        {
            Assert.IsTrue(wePlusHelper?.GetInterval() == 60, 
                $"Expected value 60, Actual value {wePlusHelper.GetInterval()}");
        }

        [TestMethod]
        public void Configuration_GetWeatherAPIKey_Test()
        {
            Assert.IsTrue(wePlusHelper?.GetWeatherAPIKey() == "3284b05d9ef64696864103019240103",
                $"Expected value '3284b05d9ef64696864103019240103', Actual value '{wePlusHelper.GetWeatherAPIKey()}'");
        }

        [TestMethod]
        public void Configuration_GetStartDate_Test()
        {
            Assert.IsTrue(wePlusHelper?.GetStartDate() == "2024-03-01",
                $"Expected value '2024-01-01', Actual value '{wePlusHelper.GetStartDate()}'");
        }

        [TestMethod]
        public void Configuration_GetWeatherAPIUrl_Test()
        {
            Assert.IsTrue(wePlusHelper?.GetWeatherAPIUrl() == "http://api.weatherapi.com/v1/",
                $"Expected value 'http://api.weatherapi.com/v1/', Actual value '{wePlusHelper.GetWeatherAPIUrl()}'");
        }

        [TestMethod]
        public void Configuration_GetOutputFolder_Test()
        {
            Assert.IsTrue(wePlusHelper?.GetOutputFolder() == "WeatherTestData",
                $"Expected value 'WeatherTestData', Actual value '{wePlusHelper.GetOutputFolder()}'");
        }

        [TestMethod]
        public async Task FetchWeatherData_Test()
        {
            await wePlusHelper.FetchWeatherData();

            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"..\\..\\..\\..\\WeatherTestData\\"));

            Assert.IsTrue(File.Exists(path + "singapore.json"),
                $"Expected value for singapore.json exists 'True', Actual value '{File.Exists(path + "singapore.json")}'");
            Assert.IsTrue(File.Exists(path + "Perth.json"),
                $"Expected value for Perth.json exists 'True', Actual value '{File.Exists(path + "Perth.json")}'");
            Assert.IsTrue(File.Exists(path + "Auckland.json"),
                $"Expected value for Auckland.json exists 'True', Actual value '{File.Exists(path + "Auckland.json")}'");

            File.Delete(path + "singapore.json");
            File.Delete(path + "Perth.json");
            File.Delete(path + "Auckland.json");
        }
    }
}