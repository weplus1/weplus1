using WePlusConsoleApp.Model;
using WePlusConsoleApp.WeatherApiClient;

namespace WePlusConsoleAppTest.WeatherApiClient
{
    [TestClass]
    public class WeatherApiClientTest
    {
        [TestMethod]
        public async Task InvokeRequestAsync_Test()
        {
            string apiUrl = "http://api.weatherapi.com/v1/";
            string apiKey = "3284b05d9ef64696864103019240103";
            string startDate = "2024-03-01";
            string endDate = "2024-03-01";
            string city = "Singapore";

            string uri = $"{apiUrl}history.json?key={apiKey}&dt={startDate}&end_dt={endDate}&q={city}&hour=1";

            var result = await WeatherClient.InvokeRequestAsync(uri);

            // You can put your assertions here

            Assert.IsNotNull(result, 
                "Data cannot be empty");

            Assert.IsInstanceOfType(result, typeof(WeatherAPIResponse),
                 $"Expected value {typeof(WeatherAPIResponse)}, Actual value {result.GetType()}");

            Assert.IsTrue("Singapore" == result.Location?.Name,
                $"Expected value 'Singapore', Actual value {result.Location?.Name}");
            Assert.IsTrue("Singapore" == result.Location?.Country,
                $"Expected value 'Singapore', Actual value {result.Location?.Country}");

            Assert.IsTrue("2024-03-01" == result.Forecast?.ForecastDay?.First().Date,
                $"Expected value '2024-03-01', Actual value {result.Forecast?.ForecastDay?.First().Date}");

            Assert.IsTrue(1709226000 == result.Forecast?.ForecastDay?.First().Hour?.First().TimeEpoch,
                $"Expected value '1709226000', Actual value {result.Forecast?.ForecastDay?.First().Hour?.First().TimeEpoch}");
            Assert.IsTrue("2024-03-01 01:00" == result.Forecast?.ForecastDay?.First().Hour?.First().Time,
                $"Expected value '2024-03-01 01:00', Actual value {result.Forecast?.ForecastDay?.First().Hour?.First().Time}");
            Assert.IsTrue(decimal.Parse("26.4").Equals(result.Forecast?.ForecastDay?.First().Hour?.First().TempC),
                $"Expected value '26.4', Actual value {result.Forecast?.ForecastDay?.First().Hour?.First().TempC}");
            Assert.IsTrue(decimal.Parse("79.6").Equals(result.Forecast?.ForecastDay?.First().Hour?.First().TempF),
                $"Expected value '79.6', Actual value {result.Forecast?.ForecastDay?.First().Hour?.First().TempF}");
        }
    }
}