﻿using System.Globalization;
using System.Text.Json.Serialization;

namespace WePlusConsoleApp.Model
{
    public class WeatherAPIResponse
    {
        [JsonPropertyName("location")]
        public Location? Location { get; set; }

        [JsonPropertyName("forecast")]
        public Forecast? Forecast { get; set; }

        public SerializeObj ToSerializedObject()
        {
            return new SerializeObj
            {
                Name = this.Location?.Name,
                ForecastDay = this.Forecast?.ForecastDay,
            };
        }
    }

    public class Location
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;


        [JsonPropertyName("country")]
        public string Country { get; set; } = string.Empty;
    }

    public class Forecast
    {
        [JsonPropertyName("forecastday")]
        public ForecastDay[]? ForecastDay { get; set; }
    }

    public class ForecastDay
    {
        [JsonPropertyName("date")]
        public string Date { get; set; } = string.Empty;

        [JsonPropertyName("hour")]
        public Hour[]? Hour { get; set; }
    }

    public class Hour
    {
        [JsonPropertyName("time_epoch")]
        public int TimeEpoch { get; set; } = default(int);

        [JsonPropertyName("time")]
        public string Time { get; set; } = string.Empty;

        [JsonPropertyName("temp_c")]
        public decimal TempC { get; set; } = default(decimal);

        [JsonPropertyName("temp_f")]
        public decimal TempF { get; set; } = default(decimal);
    }

    public class SerializeObj
    {
        [JsonPropertyName("Location")]
        public string? Name { get; set; }

        [JsonPropertyName("ForecastDay")]
        public ForecastDay[]? ForecastDay { get; set; }
    }
}
