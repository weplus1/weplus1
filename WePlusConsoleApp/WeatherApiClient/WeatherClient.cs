﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WePlusConsoleApp.Helpers;
using Microsoft.Extensions.Options;
using System.Threading;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using WePlusConsoleApp.Model;

namespace WePlusConsoleApp.WeatherApiClient
{
    public static class WeatherClient
    {
        public static async Task<WeatherAPIResponse> InvokeRequestAsync(string uri, CancellationToken cancellationToken = default)
        {
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    return await httpClient.GetFromJsonAsync<WeatherAPIResponse>(uri, cancellationToken);                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
