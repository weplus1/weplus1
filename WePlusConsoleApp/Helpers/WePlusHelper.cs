﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using WePlusConsoleApp.WeatherApiClient;

namespace WePlusConsoleApp.Helpers
{   
    public class WePlusHelper
    {
        #region Config Section
        private readonly IConfiguration _config;

        public WePlusHelper(IConfiguration config)
        {
            _config = config;
        }

        public async IAsyncEnumerable<string> GetCities()
        {
            string? cityStr =  _config.GetValue<string>("CityList");

            if (string.IsNullOrWhiteSpace(cityStr))
                throw new Exception("Countries list cannot be empty.");

            foreach (var city in cityStr.Split(','))
            {
                await Task.Delay(1000);
                yield return city;
            }
        }

        public int GetInterval()
        {
            int? interval = _config.GetValue<int>("Interval");
            return interval ?? 60;
        }

        public string GetWeatherAPIKey()
        {
            string? apiStr = _config.GetValue<string>("WeatherAPIKey");

            if (string.IsNullOrWhiteSpace(apiStr))
                throw new Exception("WeatherAPIKey cannot be empty.");

            return apiStr;
        }

        public string GetStartDate()
        {
            string? startDateStr = _config.GetValue<string>("StartDate");

            if (string.IsNullOrWhiteSpace(startDateStr))
                startDateStr = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");

            return startDateStr;
        }

        public string GetWeatherAPIUrl()
        {
            string? apiUrl = _config.GetValue<string>("WeatherAPIUrl");

            if (string.IsNullOrWhiteSpace(apiUrl))
                throw new Exception("WeatherAPIUrl cannot be empty.");

            return apiUrl;
        }

        public string GetOutputFolder()
        {
            string? folderStr = _config.GetValue<string>("OutputFolder");

            if (string.IsNullOrWhiteSpace(folderStr))
                throw new Exception("OutputFolder cannot be empty.");

            return folderStr;
        }

        #endregion

        public async Task FetchWeatherDataOnInterval()
        {
            while (true)
            {
                await FetchWeatherData();
                Thread.Sleep(1000 * 60 * GetInterval()); //This will make the console app run based on the interval, as long as the app is running. 
                //Better way to do this is to have a Worker service 
            }
        }


        public async Task FetchWeatherData()
        {
            string uri = $"{GetWeatherAPIUrl()}history.json?key={GetWeatherAPIKey()}&dt={GetStartDate()}&end_dt={DateTime.Now.ToString("yyyy-MM-dd")}&q=";

            string folderPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"..\\..\\..\\..\\{GetOutputFolder()}"));

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            await foreach (var city in GetCities())
            {
                var wheatherData = await WeatherClient.InvokeRequestAsync(uri + city);
                var wheatherDataStr = JsonSerializer.Serialize(wheatherData.ToSerializedObject());

                string filename = $"{city}.json";

                using (StreamWriter outputFile = new StreamWriter(Path.Combine(folderPath, filename)))
                {
                    await outputFile.WriteAsync(wheatherDataStr);
                }
            }
        }
    }
}

