﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using WePlusConsoleApp.Helpers;
using WePlusConsoleApp.WeatherApiClient;
using System.Net.Http.Headers;
using System.Text.Json;

namespace WePlus
{
    class Program
    {      
        static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            var helper = ActivatorUtilities.CreateInstance<WePlusHelper>(host.Services);            
            await helper.FetchWeatherDataOnInterval();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args).ConfigureAppConfiguration((context, config) =>
            {
                config.Sources.Clear();
                config.AddJsonFile("appSettings.json", optional: false, reloadOnChange: true);
            });
        }
    }
}
