import { useEffect, useState } from 'react';
import './App.css';

function App() {
    const [forecasts, setForecasts] = useState();

    //useEffect(() => {
    //    populateLocations();
    //}, []);

    useEffect(() => {
        populateWeatherData();
    }, []);

    const contents = forecasts === undefined
        ? <p><em>Loading...</em></p>
        : <table className="table table-striped" aria-labelledby="tabelLabel">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Temp. (C)</th>
                    <th>Temp. (F)</th>
                </tr>
            </thead>
            <tbody>
                {forecasts.map(forecast =>
                    <tr key={forecast.time}>
                        <td>{forecast.time}</td>
                        <td>{forecast.tempC}</td>
                        <td>{forecast.tempF}</td>
                    </tr>
                )}
            </tbody>
        </table>;

    return (
        <div>
            <h1 id="tabelLabel">Weather forecast</h1>
            <p>

                This component demonstrates fetching data from the server.</p>
            {contents}
        </div>
    );

    
    async function populateWeatherData() {
        //const requestOptions = { JSON.stringify({ location: "singapore", date: "2024-02-28" }) }
        const response = await fetch('weatherforecast?location=singapore&date=2024-02-28');
        const data = await response.json();
        setForecasts(data);
    }

    //async function populateLocations() {
    //    debugger;
    //    const response = await fetch('location/getlocations');
    //    debugger;
    //    ;
    //}
}

export default App;