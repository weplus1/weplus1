﻿using Microsoft.AspNetCore.Mvc;

namespace wepluswebserver.Controllers
{
    [ApiController]
    [Route("LocationController/GetLocations")]
    public class LocationController : ControllerBase
    {
        private readonly ILogger<LocationController> _logger;

        public LocationController(ILogger<LocationController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetLocations")]
        public IEnumerable<string> GetLocations()
        {
            return ["Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"];
        }
    }
}