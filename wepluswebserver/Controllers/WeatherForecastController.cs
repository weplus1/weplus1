using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using System.Security.Cryptography.Xml;
using System.Text.Json;
using wepluswebserver.Model;
using static System.Runtime.InteropServices.JavaScript.JSType;


namespace wepluswebserver.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast/{location}/{date}")]
        public IEnumerable<Hour> GetWeatherForecast([FromQuery]string location, [FromQuery] string date)
        {
            string filename = $"{location}.json";
            string weatherDataPath = Directory.GetParent(Directory.GetCurrentDirectory()) + new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetValue<string>("WeatherData");

            if (System.IO.File.Exists($"{weatherDataPath}\\{filename}"))
            {
                var weatherDataStr = System.IO.File.ReadAllText($"{weatherDataPath}\\{filename}");
                WeatherData? weatherData = Newtonsoft.Json.JsonConvert.DeserializeObject<WeatherData>(weatherDataStr);

                IEnumerable<int> periods = new int[] { 0, 3, 6, 9, 12, 15, 18, 21, 24 };
                return weatherData.ForecastDay.Select(s => new ForecastDay
                {
                    Hour = s.Hour.Where(si => periods.Contains(si.Time_.Hour)).ToArray()
                }).SelectMany(s => s.Hour).ToArray();
            }
            else
            {
                return null;
            }
        }
    }
}
