﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace wepluswebserver.Model
{
    public class WeatherData
    {
        [JsonProperty("Location")]
        public string Location { get; set; }

        [JsonProperty("ForecastDay")]
        public ForecastDay[] ForecastDay { get; set; }
    }

    public class ForecastDay
    {
        [JsonProperty("date")]
        public string Date { get; set; } = string.Empty;

        public DateTime Date_
        {
            get { 
                return DateTime.Parse(Date);
            }
        }

        [JsonProperty("hour")]
        public Hour[]? Hour { get; set; }
    }

    public class Hour
    {
        [JsonProperty("time_epoch")]
        public int TimeEpoch { get; set; } = default(int);

        [JsonProperty("time")]
        public string Time { get; set; } = string.Empty;

        public DateTime Time_
        {
            get
            {
                return DateTime.Parse(Time);
            }
        }

        [JsonProperty("temp_c")]
        public decimal TempC { get; set; } = default(decimal);

        [JsonProperty("temp_f")]
        public decimal TempF { get; set; } = default(decimal);
    }

    public class Weather
    {
        public string Time { get; set; } = string.Empty;
        public decimal TempC { get; set; } = default(decimal);
        public decimal TempF { get; set; } = default(decimal);
    }
}

